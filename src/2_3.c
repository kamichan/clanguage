#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{

    int i;
    char ch, buffer[512] = {0};

    do{
        // 提示语
        printf("请输入一段字符串:");
        // 如果接收失败则继续提示
    }while(!gets(buffer));

    for(i = 0; i < 512; i++) {

        ch = buffer[i];

        // '\0'结束
        if(ch == 0) {
            break;
        }

        // 转换字母大小写
        if('a' <= ch && ch <= 'z') {
            buffer[i] -= 32;
        } else if('A' <= ch && ch <= 'Z') {
            buffer[i] += 32;
        }
    }

    printf("转换字母大小写后内容: %s\n", &buffer);

    /* 防止调试器不自动中断 */
    getchar();

    return 0;
}
