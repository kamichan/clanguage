#include <stdio.h>
#include <stdlib.h>

int F(int i)
{
    return (i == 0 || i == 1) ? 1 : (F(i - 1) + F(i - 2));
}

int main(int argc, char **argv)
{

    int i;

    for(i = 0; i < 20; i++) {
        if(i != 0 && i%5 == 0) {
            printf("\n");
        }
        printf("%5d", F(i));
    }

    /* 防止调试器不自动中断 */
    getchar();

    return 0;
}
