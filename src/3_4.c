#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{

    int i, j, k, table[10][10] = {0};

    // 初始化数据
    for(i = 0; i < 10; i++) {
        for(j = 0, k = i; j < 10 - i; j++) {
            table[i][k++] = j + 1;
        }
    }

    // 输出
    for(i = 0; i < 10; i++) {
        for(j = 0; j < 10; j++) {
            printf("%5d", table[i][j]);
        }
        printf("\n");
    }

    /* 防止调试器不自动中断 */
    getchar();

    return 0;
}
