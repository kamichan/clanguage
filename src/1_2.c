#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char **argv)
{

    double x = 0.0, y;

    printf("请输入一个实数x:");
    scanf("%lf", &x);

    if(x <= 0) { // x <= 0
        y = sin(x);
    } else if(x <= 10) { // 0 < x && x <= 10
        y = exp(2 * x) + 1;
    } else { // x > 10
        y = sqrt(pow(x, 3) + pow(x, 2) + 1);
    }

    printf("x:%.4f,y:%.4f\n", x, y);

    /* 防止调试器不自动中断 */
    getchar();

    return 0;
}
