#!/bin/bash

BIN="bin"

if [ -z $1 ]; then
    echo "请指定程序源文件"
    exit -1
fi

SOURCE="./src/${1}"

if [ ! -r $SOURCE ]; then
    echo "文件${1}不可读"
    exit -1
fi

if [ ! -d ./bin ]; then
    mkdir ./bin
fi

gcc -o ./bin/$BIN $SOURCE
if [ "$?" != "0" ]; then
    echo "程序编译失败"
    exit -1
fi

# run binary
test -x ./bin/$BIN && ./bin/$BIN

# 清理执行文件
rm -rf ./bin
